<?php
/**
 * @file Commerce Order OG UI module
 */

/**
 * Implements hook_menu().
 */
function commerce_order_og_ui_menu() {
  $items = array();

  // Order listing pages.
  $items['group/%/%/admin/orders'] = array(
    'title callback' => 'og_ui_menu_title_callback',
    'title arguments' => array('Orders in group @group', 1, 2),
    'description' => 'Find and manage group orders.',
    'page callback' => 'commerce_order_og_ui_admin_orders',
    'page arguments' => array(1, 2),
    'access callback' => 'og_ui_user_access_group',
    'access arguments' => array(COMMERCE_ORDER_OG_PERM_VIEW_ORDERS, 1, 2),
    'weight' => 10,
  );

  // Order view callback
  $items['group/%/%/admin/orders/%commerce_order'] = array(
    'title callback' => 'commerce_order_ui_order_title',
    'title arguments' => array(5),
    'page callback' => 'commerce_order_og_ui_order_view',
    'page arguments' => array(1, 2, 5, 'customer'),
    'access callback' => 'og_ui_user_access_group',
    'access arguments' => array(COMMERCE_ORDER_OG_PERM_VIEW_ORDERS, 1, 2),
  );

  return $items;
}


/**
 * Form builder; OG user administration page.
 */
function commerce_order_og_ui_admin_orders($group_type, $gid) {
  og_set_breadcrumb($group_type, $gid, array(l(t('Group'), "$group_type/$gid/group")));
  // $view = explode(':', variable_get)
  list($view_name, $display) = explode(':', variable_get('commerce_order_og_admin_orders_view', 'commerce_order_og_group_orders:default'));
  // We can't use views_embed_view() as we have exposed filter, and we need to
  // make sure the path is overriden to the current URL.
  // @see http://drupal.org/node/525592#comment-1968604
  $view = views_get_view($view_name, $display);
  $view->set_arguments(array($group_type, $gid));
  $view->override_url = $_GET['q'];
  return $view->preview();
}

/**
 * Page callback for viewing a single order.
 * 
 * @param $group_type
 * @param $gid
 * @param $order
 * @param string $view_mode
 * @return bool
 */
function commerce_order_og_ui_order_view($group_type, $gid, $order, $view_mode = 'administrator') {
  // Fix breadcrumbs.
  og_set_breadcrumb($group_type, $gid, array(l(t('Group'), "$group_type/$gid/group"), l(t('View Orders'), "$group_type/$gid/group/admin/orders")));
  return commerce_order_ui_order_view($order, $view_mode);
}


/**
 * Implements hook_og_ui_get_group_admin().
 * Adds a menu item that should appear in the group admin page.
 */
function commerce_order_og_ui_og_ui_get_group_admin() {
  $items = array();
  $items['view_orders'] = array(
    'title' => t('View Orders'),
    'description' => t('View group orders'),
    'href' => 'admin/orders',
  );
  return $items;
}
