<?php
/**
 * Created by PhpStorm.
 * User: wayne
 * Date: 11/29/15
 * Time: 1:42 PM
 */


function commerce_order_og_pane_settings_form($checkout_pane) {
  $form = array();

  $form['commerce_order_og_checkout_pane_hide_empty'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide checkout pane if user has no groups they can create orders for'),
    '#default_value' => variable_get('commerce_order_og_checkout_pane_hide_empty', TRUE),
  );
  
  $form['commerce_order_og_checkout_pane_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Fieldset Title'),
    '#default_value' => variable_get('commerce_order_og_checkout_pane_title', t('Group')),
  );

  $form['commerce_order_og_checkout_pane_field_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Group Field Label'),
    '#default_value' => variable_get('commerce_order_og_checkout_pane_field_label', t('Group')),
  );

  $form['commerce_order_og_checkout_pane_ajax_reload'] = array(
    '#type' => 'checkbox',
    '#title' => t('Reload checkout page with AJAX on group change'),
    '#description' => t('This is useful if you have pricing rules or payment options that are affected by the order group.'),
    '#default_value' => variable_get('commerce_order_og_checkout_pane_ajax_reload', FALSE),
  );
  $form['commerce_order_og_checkout_pane_ajax_message'] = array(
    '#type' => 'textfield',
    '#title' => t('AJAX Progress Message'),
    '#description' => t('The message to show while the AJAX submissions is processing.'),
    '#default_value' => variable_get('commerce_order_og_checkout_pane_ajax_message', t('Please wait...')),
    '#states' => array(
      'visible' => array(
        ':input[name="commerce_order_og_checkout_pane_ajax_reload"]' => array('checked' => TRUE),
      ),
    ),
  );
  
  return $form;
}


function commerce_order_og_pane_checkout_form(&$form, &$form_state, $checkout_pane, $order) {
  $pane_form = array();
  $og_content_field = field_info_field(COMMERCE_ORDER_OG_AUDIENCE_FIELD);

  $gids = commerce_order_og_get_possible_order_groups_for_user(
    $order->uid, 
    $og_content_field['settings']['target_type'],
    $og_content_field['settings']['handler_settings']['target_bundles']
  );
  if (!empty($gids)) {
    $entities = entity_load($og_content_field['settings']['target_type'], $gids);
    $options = array('' => '');
    foreach ($entities as $entity) {
      $options[entity_id($og_content_field['settings']['target_type'], $entity)] = entity_label($og_content_field['settings']['target_type'], $entity);
    }
    
    $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
    $pane_form['commerce_order_og_target_id'] = array(
      '#type' => 'select',
      '#title' => variable_get('commerce_order_og_checkout_pane_field_label', t('Group')),
      '#options' => $options,
      '#default_value' => $order_wrapper->{COMMERCE_ORDER_OG_AUDIENCE_FIELD}->raw(),
      '#id' => 'commerce-order-og-target-id',
    );
  }
  
  return $pane_form;
}

function commerce_order_og_ajax_reload(&$form, &$form_state) {
  watchdog('debug', REQUEST_TIME . ': commerce_order_og_ajax_reload');
  $order_wrapper = entity_metadata_wrapper('commerce_order', $form_state['order']);
  if (!empty($form_state['values']['commerce_order_og']['commerce_order_og_target_id'])) {
    $order_wrapper->{COMMERCE_ORDER_OG_AUDIENCE_FIELD} = $form_state['values']['commerce_order_og']['commerce_order_og_target_id'];
  }
  else {
    $order_wrapper->{COMMERCE_ORDER_OG_AUDIENCE_FIELD} = NULL;
  }
  
  /* 
   * Note: The below code is the result of hours of banging my head against the
   * wall of commerce price calculations. Frankly, I have no idea why these 
   * specific steps work, but they do for me. Patches welcome, if you understand
   * it better. 
   * 
   * The steps below do the requisite saving and reloading to allow you to refresh
   * the entire checkout page taking into account the group membership that the user
   * selected.
   */
  
  // Save order.
  $order_wrapper->save();
  
  // Force recalculation of all product-bearing line items.
  foreach ($order_wrapper->commerce_line_items as $li_wrapper) {
    if (isset($li_wrapper->commerce_product)) {
      $li_wrapper->commerce_unit_price = commerce_product_calculate_sell_price($li_wrapper->commerce_product->value());
      $li_wrapper->save();
    }
  }
  // Save again. Why? F**k knows. Your prices will be one step behind if you don't, though.
  $order_wrapper->save();

  // It's dangerous to go alone! Take this.
  module_invoke_all('commerce_order_og_ajax_reload', $form, $form_state);
  
  /* 
   * Rebuild the form. Nope, $form_state['rebuild'] does not work here for some reason.
   * And, this ajax call happens after other panes have been built, so if you don't 
   * rebuild the form like this, your prices will be one step behind.
   */
  $form = drupal_rebuild_form($form_state['build_info']['form_id'], $form_state, $form);
  
  return $form;
}


function commerce_order_og_pane_checkout_form_submit($form, &$form_state, $checkout_pane, $order) {
  if (!empty($form_state['values']['commerce_order_og']['commerce_order_og_target_id'])) {
    $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
    $order_wrapper->{COMMERCE_ORDER_OG_AUDIENCE_FIELD} = $form_state['values']['commerce_order_og']['commerce_order_og_target_id'];
  }
}
  
function commerce_order_og_pane_review($form, $form_state, $checkout_pane, $order) {
  $content = array();
  $og_content_field = field_info_field(COMMERCE_ORDER_OG_AUDIENCE_FIELD);
  if (!empty($og_content_field)) {
    $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
    $group = $order_wrapper->{COMMERCE_ORDER_OG_AUDIENCE_FIELD}->value();
    $content[] = array(
      '#type' => 'item',
      '#title' => variable_get('commerce_order_og_checkout_pane_field_label', t('Group')),
      '#markup' => check_plain(entity_label($og_content_field['settings']['target_type'], $group)),
    );
  }
  return drupal_render($content);
}
